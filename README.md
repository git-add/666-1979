# 666-1979

`8 books` `all files encrypted`

---

[Introduction to Mathematical Logic](./bok%253A978-1-4613-9441-9.zip)
<br>
Set Theory Computable Functions Model Theory
<br>
Jerome Malitz in Undergraduate Texts in Mathematics (1979)

---

[Join Geometries](./bok%253A978-1-4613-9438-9.zip)
<br>
A Theory of Convex Sets and Linear Geometry
<br>
Walter Prenowitz, James Jantosciak in Undergraduate Texts in Mathematics (1979)

---

[Elementary Probability Theory with Stochastic Processes](./bok%253A978-1-4684-9346-7.zip)
<br>
Kai Lai Chung in Undergraduate Texts in Mathematics (1979)

---

[A Concrete Introduction to Higher Algebra](./bok%253A978-1-4684-0065-6.zip)
<br>
Lindsay Childs in Undergraduate Texts in Mathematics (1979)

---

[Calculus: An Historical Approach](./bok%253A978-1-4684-9349-8.zip)
<br>
William McGowen Priestley in Undergraduate Texts in Mathematics (1979)

---

[Elementary Topics in Differential Geometry](./bok%253A978-1-4612-6153-7.zip)
<br>
J. A. Thorpe in Undergraduate Texts in Mathematics (1979)

---

[Dynamic Topology](./bok%253A978-1-4684-6262-3.zip)
<br>
Gordon Whyburn, Edwin Duda in Undergraduate Texts in Mathematics (1979)

---

[Much Ado About Calculus](./bok%253A978-1-4615-9644-8.zip)
<br>
A Modern Treatment with Applications Prepared for Use with the Computer
<br>
Robert L. Wilson in Undergraduate Texts in Mathematics (1979)

---
